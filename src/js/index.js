import styleSelect from 'styleselect';
import 'styleselect/css/styleselect.css';
import { tns } from 'tiny-slider/src/tiny-slider';
import 'tiny-slider/dist/tiny-slider.css';
import check_if_in_view from './animation';

let header = document.querySelector('.header');

styleSelect('select');

tns({
    'container': '.hero-slider',
    'items': 1,
    'speed': 500,
    'mode': 'gallery',
    'arrowKeys': false,
    'autoplay': true,
    'autoplayHoverPause': false,
    'autoplayTimeout': 5000,
    'swipeAngle': false
});

tns({
    'container': '.portfolio-slider',
    'items': 1,
    'speed': 500,
    'mode': 'gallery',
    'arrowKeys': false,
    'autoplay': true,
    'autoplayHoverPause': false,
    'autoplayTimeout': 5000,
    'swipeAngle': false
});

let headerBg = () => {
    let scrollTop = window.pageYOffset;
    if (scrollTop > 200) {
        header.classList.add('active');
    } else {
        header.classList.remove('active');
    }
};

window.onscroll = () => {
    headerBg();
    check_if_in_view();
};

let scrollTo = (element, to, duration) => {
    if (duration <= 0) return;
    var difference = to - element.scrollTop;
    var perTick = difference / duration * 10;

    setTimeout(function () {
        element.scrollTop = element.scrollTop + perTick;
        if (element.scrollTop === to) return;
        scrollTo(element, to, duration - 10);
    }, 10);
};

let linkNav = document.querySelectorAll('[href^="#"]'),
    speed = 0.5;

linkNav.forEach((element, i) => {
    element.addEventListener('click', function (e) {
        e.preventDefault();
        let windowTop = window.pageYOffset,
            hash = this.href.replace(/[^#]*(.*)/, '$1'),
            positionBlock = document.querySelector(hash).getBoundingClientRect().top,
            start = null;
        requestAnimationFrame(step);
        function step (time) {
            if (start === null) start = time;
            var progress = time - start,
                r = (positionBlock < 0 ? Math.max(windowTop - progress / speed, windowTop + positionBlock) : Math.min(windowTop + progress / speed, windowTop + positionBlock));
            window.scrollTo(0, r);
            if (r != windowTop + positionBlock) {
                requestAnimationFrame(step);
            } else {
                location.hash = hash;
            }
        }
    }, false);
});
