let animation_elements = document.querySelectorAll('.animation-element');

export default function check_if_in_view () {
    let window_height = window.innerHeight,
        window_top_position = window.pageYOffset,
        window_bottom_position = (window_top_position + window_height);
    animation_elements.forEach(function (element) {
        let element_height = element.offsetHeight,
            element_top_position = element.offsetTop,
            element_bottom_position = (element_top_position + element_height);

        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
            element.classList.add('in-view');
        }
        else {
            element.classList.remove('in-view');
        }
    });
}
// document.addEventListener("DOMContentLoaded", window.scrollTo(0,1));